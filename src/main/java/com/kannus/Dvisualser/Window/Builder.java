package com.kannus.Dvisualser.Window;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import com.jogamp.newt.event.KeyEvent;
import com.kannus.Dvisualser.Loader.load3D;

public class Builder {
	
	
	//Container for the list of 3D filenames in the model folder
	private List<String> fileNames=new ArrayList<String>();
	
	//Constructor
	public Builder(){
		this.readFileNames();
	}
	
	//Getting the filenames of the available 3D models
	private void readFileNames() {
		File folder = new File("./model");
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				System.out.println("File " + listOfFiles[i].getName());
				this.fileNames.add(listOfFiles[i].getName());
		    }
			else if (listOfFiles[i].isDirectory()) {
				System.out.println("Directory " + listOfFiles[i].getName());
			}
		}
	}
	
	//Adding model names to the menu 
	private void buildMenuItems(JMenu menu) {
	    if(fileNames.size() == 0 || fileNames.isEmpty() || fileNames== null){
	        throw new RuntimeException("No .obj file found in the model folder!");
	    }
	    else
	    	this.fileNames.stream()
			.forEach(fileName -> {
				JMenuItem menuItem = new JMenuItem(new AbstractAction(fileName) {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent e) {
						new load3D(fileName);	
					}
				} );
				menuItem.setAccelerator(KeyStroke.getKeyStroke(
				        KeyEvent.VK_1, ActionEvent.ALT_MASK));
				menu.add(menuItem);
			});
	}
		
	//creating GUI
	public void createAndShowGUI() {
		//Create and set up the window.
		JFrame frame = new JFrame("3D visualizer");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		//Create the menu bar
		JMenuBar menuBar = new JMenuBar();

		//Build the first menu.
		JMenu menu = new JMenu("Models");
		menu.setMnemonic(KeyEvent.VK_A);
		menu.getAccessibleContext().setAccessibleDescription(
		        "Menu item containing the names of the available 3D models");
		menuBar.add(menu);
		frame.setJMenuBar(menuBar);
		
		this.buildMenuItems(menu);
		
		frame.setSize(400, 600);
		
		//frame.pack(); //pack fits the window 
		frame.setVisible(true);
		}

}
