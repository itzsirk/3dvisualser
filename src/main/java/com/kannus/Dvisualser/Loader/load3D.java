package com.kannus.Dvisualser.Loader;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.Background;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.Sensor;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

import com.sun.j3d.loaders.IncorrectFormatException;
import com.sun.j3d.loaders.ParsingErrorException;
import com.sun.j3d.loaders.Scene;
import com.sun.j3d.loaders.objectfile.ObjectFile;
import com.sun.j3d.utils.behaviors.sensor.Mouse6DPointerBehavior;
import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.universe.ConfiguredUniverse;
import com.sun.j3d.utils.universe.ViewingPlatform;

public class load3D {
	
	  private String fileName=null;	   
	  private URL configURL=null;
	  private double creaseAngle = 60.0; //crease angle for normal generation
	  private ConfiguredUniverse cu;
	  
	  //Constructor
	  public load3D(String fileName) {
		  this.fileName=fileName;
		  init();
	  }
	  
	  //Initializer
	  public void init() {

		    //Get the configURL from the j3d.configURL
		    //Or use the default config file "j3d1x1-window" in the conf directory
		    if(configURL==null) {
		        try {
		        	//URL url = getClass().getResource("j3d1x1-window.cfg");
		        	//File myFile = new File(url.getPath());
		        	ClassLoader classLoader = this.getClass().getClassLoader();
		        	File myFile = new File(classLoader.getResource("./j3d1x1-window.cfg").getFile());
		        	configURL=myFile.toURI().toURL();
		        } catch (MalformedURLException e) {
		          System.err.println(e);
		          System.exit(1);
		        }
		    }

		    //Create a simple scene and attach it to the virtual universe
		    BranchGroup scene = createGraph();
		    cu = new ConfiguredUniverse(configURL);

		    //Getting the ViewingPlatform and setting the appropriate field of view
		    ViewingPlatform viewingPlatform = cu.getViewingPlatform();
		    viewingPlatform.setNominalViewingTransform();

		    //Adding a ViewPlatformBehavior if not specified in the config file
		    if ( viewingPlatform.getViewPlatformBehavior() == null) {
			      OrbitBehavior orbit = new OrbitBehavior(cu.getCanvas(), OrbitBehavior.REVERSE_ALL);
			      BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);
			      orbit.setSchedulingBounds(bounds);
			      viewingPlatform.setViewPlatformBehavior(orbit);
			    }

		    //Mouse behavior
		    Map<?,?> sensorMap = null;
		    sensorMap = cu.getNamedSensors();
		    //See if there's a 6 degree of freedom mouse in the environment
		    if (sensorMap != null) {
		      Sensor mouse6d = (Sensor) sensorMap.get("mouse6d");
		      if (mouse6d != null) {
		    	//Defining mouse behavior
		        Mouse6DPointerBehavior behavior = new Mouse6DPointerBehavior(mouse6d, 1.0, true);
		        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);
		        behavior.setSchedulingBounds(bounds);
		        //adding to the scene
		        scene.addChild(behavior);
		        scene.addChild(behavior.getEcho());
		      }
		    }

		    
		    //Listen for a typed "q", "Q", or "Escape" key to exit
		    Canvas3D[] canvases;
		    canvases = cu.getViewer().getCanvas3Ds();
		    class QuitListener extends KeyAdapter {
		      public void keyTyped(KeyEvent e) {
		        char c = e.getKeyChar();
		        if (c == 'q' || c == 'Q' || c == 27)
		          System.exit(0);
		      }
		    }
		    QuitListener quitListener = new QuitListener();
		    for (int i = 0; i < canvases.length; i++)
		      canvases[i].addKeyListener(quitListener);

		    cu.addBranchGraph(scene);   
		  }

	  //Scene graph creator
	  public BranchGroup createGraph() {
	    // Root of the graph
	    BranchGroup modelRoot = new BranchGroup();

	    //Scaling 
	    Transform3D transf3D = new Transform3D();
	    transf3D.setScale(0.8);
	    TransformGroup modelScale = new TransformGroup();
	    modelScale.setTransform(transf3D);
	    modelRoot.addChild(modelScale);

	    //Creating the transform group node
	    TransformGroup modelTransf = new TransformGroup();
	    //Allowing runtime modification
	    modelTransf.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
	    modelTransf.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
	    //Adding to the graph
	    modelScale.addChild(modelTransf);

	    int flags = ObjectFile.RESIZE;
	    ObjectFile f = new ObjectFile(flags, (float) (creaseAngle * Math.PI / 180.0));
	    
	    //Creating scene
	    Scene s = null;
	    try {
	      //s = f.load(fileURL);
	      s = f.load("./model/"+fileName);
	    } catch (FileNotFoundException e) {
	      System.err.println(e);
	      System.exit(1);
	    } catch (ParsingErrorException e) {
	      System.err.println(e);
	      System.exit(1);
	    } catch (IncorrectFormatException e) {
	      System.err.println(e);
	      System.exit(1);
	    }
	    //Adding to the graph
	    modelTransf.addChild(s.getSceneGroup());
	    //Creating bounding for the model
	    BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);

	    //Setting up the background and lighting of the scene
	    //Background
	    Color3f bgColor = new Color3f(0.0f, 0.0f, 0.0f);
	    Background bgNode = new Background(bgColor);
	    bgNode.setApplicationBounds(bounds);
	    modelRoot.addChild(bgNode);
	    
	    //Ambient light
	    Color3f ambientColor = new Color3f(0.1f, 0.1f, 0.1f);
	    AmbientLight ambientLight = new AmbientLight(ambientColor);
	    ambientLight.setInfluencingBounds(bounds);
	    modelRoot.addChild(ambientLight);
	    
	    //Directional light 1
	    Color3f light1Color = new Color3f(1.0f, 1.0f, 0.9f);
	    Vector3f light1Dir = new Vector3f(1.0f, 1.0f, 1.0f);
	    DirectionalLight light1 = new DirectionalLight(light1Color,light1Dir);
	    light1.setInfluencingBounds(bounds);
	    modelRoot.addChild(light1);
	    
	    //Directional light 2
	    Color3f light2Color = new Color3f(1.0f, 1.0f, 1.0f);
	    Vector3f light2Dir = new Vector3f(-1.0f, -1.0f, -1.0f);
	    DirectionalLight light2 = new DirectionalLight(light2Color,light2Dir);
	    light2.setInfluencingBounds(bounds);
	    modelRoot.addChild(light2);

	    return modelRoot;
	  }

	  //Terminal usage help
	  /*
	  private void usage() {
	    System.out.println("Usage: java ObjLoad <.obj file>");
	    System.exit(0);
	  }
	  */
}

