package com.kannus.Dvisualser;

import com.kannus.Dvisualser.Window.Builder;

/**
 * 3D visualizer 
 * Capable of opening *.obj files contained in the model folder
 */
public class App 
{
    public static void main( String[] args )
    {
    	Builder builder=new Builder();
    	 javax.swing.SwingUtilities.invokeLater(new Runnable() {
    		 public void run() {
    			 builder.createAndShowGUI();
    		 }
    	});
    }
}
